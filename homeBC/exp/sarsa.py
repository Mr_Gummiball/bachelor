
'''
Created on 30.04.2014

@author: Jonas
'''
'''obsub see: https://github.com/aepsil0n/obsub/blob/master/README.rst'''

import numpy
import obsub

import BackProp
import transferFuncCollection as tf
import world as world  # for python 27: import world27 as world


class MLP(object):
    
    def __init__(self, params):
        '''
        @param params: the DNA as a dictionary
        '''
        super().__init__()
        self.gamma =params.get("discountFactor")
        self.learningRate=params.get("learningRate")
        self.sizeX, self.sizeY = params.get("worldSize")
        self.reward=params.get("reward")
        self.punishment=params.get("punishment")
        self.layerSize= (2, params.get("hiddenLayerSize") ,4)
        self.transF=params.get("transferFunc")
        self.runs = params.get("runs")
        self.ra=params.get("reliability")
        self.weightStrad=params.get("weightInit")
        self.wMin=params.get("weightInitMin")
        self.momentum=params.get("momentum")
        self.error=[]
        self.deaths=0
        #used for return form of the world interface
        self.needsFlat=False
        self.danger=params.get("danger")
        if self.danger:
            self.interface = world.dangerWorld(self.sizeX,self.sizeY, self.reward, self.punishment, die=True)
        else:
            self.interface = world.world(self.sizeX,self.sizeY)
        self.wc = BackProp.NN(self.layerSize, self.weightStrad, self.transF,self.wMin)
        print("sarsa started")
    @obsub.event
    def q_values(self, arg):
        None
        
    @obsub.event
    def punish(self, arg):
        None
    @obsub.event
    def toLearnTerm(self, arg):
        a=arg[0]
        b=arg[1]
        arg=a-b
        None
        
    def into_s(self, nn_out):
        return numpy.resize(nn_out, (nn_out.shape[1]))
    
    def intoAS(self, s):
        return s
    def intoQ(self, s,a):
        return s[numpy.nonzero(a)[0]]
    def intoGoal(self, qStrisce ,r):
        return self.gamma*qStrisce +r
    def intoTrainTerm(self,a,q,goal):
        term=a*(q-goal)
        return numpy.resize(term, (len(term), 1))
    
    def run(self):
        '''
        activates the sarsa algorithm for the amount of runs, defined in the @params
        '''
        #interface=interface.world(sizeX,sizeY)
        maxR = self.reward
        for i in range (1, self.runs):
            err=0
            self.interface.initialise()
            '''
            path is used to save the history
            '''
            #old state
            state=self.interface.getState(flat=self.needsFlat)
            nn_out=self.wc.Run(numpy.resize(state, (1,len(state))))
            s=self.into_s(nn_out)
            #old action
            self.q_values((s,self.interface.getState(flat=True)))
            a = tf.softmax(self.intoAS(s), self.ra)
            #old q-value
            q = self.intoQ(s,a)
            iter=0
            r=0
            rSum = 0
            deadCounter=0
            #debug:
            goal=0
            
            while (r==0):
                iter += 1
                
                #act
                self.interface.action(a)
                #observe
                r = self.interface.getReward()
                #collecting information
                rSum +=r
                if (r<0):
                    deadCounter +=1
                    self.deaths+=1
                    self.punish(True)
                else:
                    self.punish(False)
                
                #new state
                stateStrisce=self.interface.getState(flat=self.needsFlat)
                #MLP output for new state
                nn_out= self.wc.Run(numpy.resize(stateStrisce, (1,len(stateStrisce))))
                sStrisce = self.into_s(nn_out)
                #update state for listeners
                self.q_values((sStrisce,self.interface.getState(flat=True)))
                #new action

                aStrisce = tf.softmax(self.intoAS(sStrisce), self.ra)
                #new q-value
                qStrisce = self.intoQ(sStrisce, aStrisce)
                
                #predict Error new 
                if (r == maxR):
                    goal= r                      
                else: 
                    goal =self.intoGoal(qStrisce,r)
                #update momentum
                moment=self.momentum**iter

                #wc has it's self._layer_output list for s'. If i train there it, trains the Q(s_t+1,a) state and not (what is right( Q(s,a)).
                #therefore, i have to run wc with the last state, to reset the Q-s value.
                self.wc.Run(numpy.resize(state, (1,len(state))))
                #end 'debug', continue as usual
                #train
                trainterm=self.intoTrainTerm(a, q, goal)
                
                #update information for listener
                self.toLearnTerm((q,goal))
                #train
                err += numpy.absolute(self.wc.train(trainterm, trainingRate=self.learningRate*moment))
                
                #old becomes new
                state=stateStrisce
                s = sStrisce
                a = aStrisce
                q = qStrisce
                if (iter>10000):
                    #wc.visual()
                    print ("not reached")
                    return self.runs*2 #artificial punishment
        #print("sarsa ended with fitness:", self.deaths)
        return self.deaths    


class table(MLP):
    '''
    inherits  the run method from the MLp sarsa (Class MLP).
    Transforms params into table, to be correct for the NN
    '''
    def __init__(self, params):
        super().__init__(params)
        self.wc=BackProp.NN(((self.sizeX+1)*(self.sizeY+1) ,4), self.weightStrad, self.transF,self.wMin)
        for weight in self.wc.weights:
            weight*=0
        '''reinitialize the Neural Network'''
        #TODO: maybe find better way to init NN   
        self.needsFlat=True

class Separated(MLP):
    '''
    inherits  the run method from the MLp sarsa (Class MLP).
    Transforms params into table, to be correct for the NN
    '''
    
    def __init__(self, params):
        super().__init__(params)
        self.wc=BackProp.AmygdalaNN(self.layerSize)
        '''function, target'''
        self.punish+=self.wc.set_fear
        
        
    @obsub.event
    def q_values(self, arg):
        q=arg[0][0]-arg[0][1]
        arg=(q,arg[1:])
        None
    @obsub.event
    def toLearnTerm(self, arg):
        if type(arg[1])==type(0) or type(arg[1])==type(0.12454):
            a=arg[0][0]-arg[0][1]
            b=arg[1]
        else:
            a=arg[0][0]-arg[0][1]
            b=arg[1][0]-arg[1][1]
        arg=a,b
        None
    def into_s(self, nn_out):
        return MLP.into_s(self, nn_out[0]),MLP.into_s(self, nn_out[1])
    
    def intoAS(self, s):
        return MLP.intoAS(self, s[0])+MLP.intoAS(self, s[1])
    
    def intoQ(self, s, a):
        return MLP.intoQ(self, s[0], a),MLP.intoQ(self, s[1], a)

    def intoGoal(self, qStrisce, r):
        if r<0:
            #death
            
            fear_return =MLP.intoGoal(self, qStrisce[1], r)
            #reward neutral
            reward_return=MLP.intoGoal(self, qStrisce[0], 0)
        else:
            #no death, zero learning might lead to fear forgetting
            fear_return =MLP.intoGoal(self, qStrisce[1], 0)
            #reward reached, or at least no negative experience
            #TD step learning from t-1 q-values
            #on every non-fear tile, the external stimuli for the Amygdala is zero
            reward_return=MLP.intoGoal(self, qStrisce[0], r)
        return reward_return, fear_return
    
    def intoTrainTerm(self, a, q, goal):
        if type(goal)==type(0) or type(goal)==type(0.12454):
            #reward reached, goal is type of integer or float
            term_r=MLP.intoTrainTerm(self, a, q[0], goal)
            term_f=MLP.intoTrainTerm(self, a, q[1], 0)
        else:
            #reward not reached, or fear reached. multiple "goals"
            term_r=MLP.intoTrainTerm(self, a, q[0], goal[0])
            term_f=MLP.intoTrainTerm(self, a, q[1], goal[1])
        return (term_r, term_f)
    
    
if __name__=="__main__":
    import argparse
    from PropertiesManager import Properties
    props=Properties(random=True)
    parser = argparse.ArgumentParser(description='arguments: MLP or table look-up version and default paramDict')
    parser.add_argument("algoType", help="0=MLP, 1=TB, 2 separated",
                type=int)
    parser.add_argument("paramDict", help="0=random, or 1=default",
                type=int)
    parser.add_argument("dangerous_World", help="Should the intellect receive a punishment? 1 for yes, 0 for no",
                type=int)
    args = parser.parse_args()
    if args.dangerous_World==1:
        danger=True
    elif args.dangerous_World==0:
        danger=False
    if args.paramDict==1:
        props=Properties(random=False)
    elif args.paramDict==0:
        props=Properties(random=True)
    props.paramDict['danger']=danger
    if args.algoType==0:
        sas=MLP(props.paramDict)
    elif args.algoType==1:
        sas=table(props.paramDict)
    elif args.algoType==2:
        sas=Separated(props.paramDict)
    print ('properties: ', props.paramDict)
    print("deaths: ", sas.run())
