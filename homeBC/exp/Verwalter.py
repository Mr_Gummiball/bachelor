'''
Created on 07.11.2014

@author: Jonas
'''
import argparse
from pymongo import MongoClient, ASCENDING

import PropertiesManager as PM
import evolution
'''
classdoc

The "Verwalter" is manages read and write operations to the data base.
It's purpose is:1. To load a generation from the database into a genetic algorithm (GA).
                2. To save to GAs progression in the data base
The program connects to 
'''

def load_pop(d_b, pop_size):
    alive_population=d_b.find({"fitness":{"$lt":1000000000}}).sort([('fitness', ASCENDING)])
    winner_pop=[]
    iter=1
    for load_indi in alive_population:
        iter+=1
        #hiereeee
        indi=PM.Properties(paramDict=load_indi)
        indi.from_json()
        winner_pop.append(indi)
        if iter>=pop_size:
            return winner_pop
    return winner_pop

def start(runs=10, populationSize=100, deaths=5, experiment='MLP', client='myClient'):
    '''init col and load correct bib'''
    mcl=MongoClient()
    db=mcl.Client[client]
    col=db[experiment]
    print("opened collection: ",col)
    '''load best population'''
    winner_pop=load_pop(col, populationSize)
    '''init GA'''
    try:
        evo=evolution.myEvoluction(population=winner_pop,maxDeath=deaths, algoType=experiment,populationSize=populationSize)
    except LookupError as e:
        print(e)
        return
    for i in range(0,runs):
        print("run:",i)
        population=evo.run_generation()
        for indi in population:
            commit_indi=indi.getCopy().to_json()
            '''to not end up having duplicate ids'''
            _id= col.insert(commit_indi)
            #print("inserted", _id)
            commit_indi={}


if __name__=="__main__":
    parser = argparse.ArgumentParser(description='-h for help and to list the arguments')
    parser.add_argument("experiment", help="Which Experiment Do You want to start? \"mlP\" or \"talbe\" or separated",
                type=str)
    parser.add_argument("database", help="Which database Do You want to use? The collection is equal to the selected experiment",
                type=str)
    parser.add_argument("runs", help="How many evolutionary runs do You want to make?",
                type=int)
    parser.add_argument("populationSize", help="The amount of individuals You want to train",
                type=int)
    args = parser.parse_args()
    start(runs=args.runs, populationSize=args.populationSize, experiment=args.experiment, client=args.database)
