'''
Created on 28.11.2014

@author: Jonas
'''
import argparse
from multiprocessing import Pool
import multiprocessing

from PropertiesManager import Properties as props
import numpy as np
import sarsa


def Evalfitness(toPass):
    '''
    computes the cost factor
    @param algo: the sarsa algo object
    @param params: the genome to process
    '''
    algo, individial=toPass
    sarsa=algo(individial.getReflection())
    return sarsa.run()


class myEvoluction(object):
    '''
    classdocs
    run main method with arguments to specify 1. |population| 2. # of runs 3. deathtrigger (1-5)
    '''


    def __init__(self, population, maxDeath, algoType, populationSize=100):
        '''
        Constructor
        @param population: existing population(can be emtpy list)
        @param maxDeath: number of death for one death-tile until fitness=max
        @param algoType: "table", or "MLP", for table based sarsa or Multilayer-Perceptron
        @param populationSize: Max population size.
        '''
        self.population=population
        self.maxDeath=maxDeath
        
        self.pool= Pool(processes=(multiprocessing.cpu_count() - 2))
        
        '''list of tupels, first entry has parameter object, second its fitness'''
        if algoType=="table":
            self.MLP=sarsa.table
            print ("lookup sarsa activated")
        elif algoType=="MLP":
            self.MLP=sarsa.MLP
            print ("MLP sarsa activated")
        elif algoType=="separated":
            self.MLP=sarsa.Separated
            print ("separated MLP sarsa activated")
        else:
            raise LookupError("No valid experiment found. Your choices are: \"table\" \"MLP\" and  \"separated\".")

        '''fill populatio with new'''
        for i in range (len(population),populationSize):
            newIndi=props(maxDeaths=maxDeath)
            self.population.append(newIndi)
        

    

    def measure_fitness(self):
        #fitnessArray=[]
        '''line up queue'''
        workerList=[]
        for individual in self.population:
            workerList.append([self.MLP, individual])
        #manages processes
        fitnessArray=self.pool.map(Evalfitness, workerList)
            
        #update indies
        for (individual,fitness) in zip(self.population,fitnessArray):
            individual.paramDict.update({"fitness":fitness})
            
        return np.array(fitnessArray)
    
    def produce_children(self, parent_list):
        childList=[]
        half=int(len(parent_list))
        for (parentA,ParantB) in zip(parent_list[0:],parent_list[half:]):
            '''create child'''
            child=parentA.getCopy()
            child.merge(ParantB)
            '''mutate'''
            mutation=np.random.random()>0.5
            if mutation:
                params=list(child.meta)
                mutated_param=params[np.random.randint(0, high=len(params)-1)]
                child.flipp([mutated_param])
            childList.append(child)
        return childList              

    def chooseParents(self, repoHill):
        parentList=[]
        for indi in self.population:
            if indi.getFitness()>repoHill:
                parentList.append(indi)
        #a_parent=parentList[:int(len(parentList)/2)]
        #b_parent=parentList[int(len(parentList)/2+1):]
        return parentList
    def run_generation(self):
        print("running gen. Algo:", self.MLP, "population", len(self.population))
        '''25% of the lowest Fitness die, 50%best create children, chance of 0.5 one param flipps'''
        fa=self.measure_fitness()
        fa.sort()
        repo_selected=int(len(fa)/2)
        repoHill=(fa[0:repo_selected]).argmax()
        '''worst died'''
        parentList=self.chooseParents(repoHill)
        childList=self.produce_children(parentList)
        print("ran ",  self.MLP)
        '''now i have the children, time to replace the dead ones'''
        for child in childList:
            weakest= fa.argmax()
            self.population[weakest]=props(paramDict=child.getReflection())
            fa[weakest]=0
        return self.population
     
'''run this file from your console.'''
if __name__=="__main__":
    parser = argparse.ArgumentParser(description='arguments: population, number of runs, deaths. Integer values separated by space')
    parser.add_argument("pop", help="number of individuals in each generation the GA shall have.",
                type=int)
    parser.add_argument("runs", help="number of generations that shall be computed.",
                type=int)
    parser.add_argument("MLP", help="table or MLP for the type of algorithm",
                type=str)
    args = parser.parse_args()
    evo=myEvoluction(population=[], maxDeath=5, algoType=args.MLP, populationSize=args.pop)
    for i in range(0,args.runs):
        evo.run_generation()
    
                
                