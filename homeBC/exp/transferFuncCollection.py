'''
Created on 02.11.2014

@author: Jonas

collection of the available transfer functions, as well as the action selection.
'''
import unittest

import numpy as np


def tanghyper(x, Derivative=False):
    '''tanh transfer-function'''
    if not Derivative:
        return np.tanh(x)
    else:
        out=tanghyper(x)
        return (1-out**2)

def sgm(x, Derivative=False):
    '''sigmoide transfer-function'''
    if not Derivative:
        return 1/(1+np.exp(-x))
    else:
        out=sgm(x)
        return out*(1-out)

def softmax(h, ra):
    '''Boltzmann-normalized softmax'''
    #evaluated, works as it should be
    #normalize with Boltzmann
    #assert h>0, "no negative values allowed?!"
    #assert 0<ra<1000, "max of ra is 1000"
    assert len(h)%4==0, "MLP with ones(len(output))/len(output)!=1 is not allowed"
    try:
        summe = np.exp(ra*(1+h)).sum()
        valueArray = np.exp(ra*(1+h)) /summe
    except:
        dummy_ar=np.array(h)
        answerArray = np.zeros(len(h))
        answerArray[dummy_ar.argmax()] = 1
    #pick value according to its probability
    if np.isnan(valueArray).any():
        valueArray=np.ones(len(valueArray)) /len(valueArray)
    rd = np.random.choice(h,replace=False, p= valueArray)
    #select proper number in array of chosen value
    selectedAction = np.where(h ==rd)[0][0]
    answerArray = np.zeros(len(h))
    answerArray[selectedAction] = 1
    #return array, with chosen number
    return answerArray


class TestSoftmax(unittest.TestCase):
    def setUp(self):
        self.ra=np.random.randint(100000)
        self.h=self.make_h(np.random.random(size=(4)))
        
    def make_h(self, h):
        case_neg=np.random.random()>0.5
        case_toBig=np.random.random()>0.5
        case_toMany=np.random.random()>0.5
        if case_toMany:
            bigger_s=np.random.randint(1000)
            h=np.concatenate([h, np.random.random(size=(bigger_s))],axis=0)
        if case_neg:
            h=h*-1
        else:
            if case_toBig:
                h=h*np.random.randint(10000)
        return h
        
    def test_softmax(self):
        answerArray=softmax(self.h, self.ra)
        self.assertTrue(answerArray.sum()==1)
        
if __name__=="__main__":
    for k in range(0,10000000):
        unittest.main()