'''
Created on Dec 31, 2014

@author: Jonas


with the help of   Ryan Harris' totorial
(https://www.youtube.com/user/nqramjets)
'''
import obsub

import numpy as np
import transferFuncCollection as transs


class NN(object):
    """Back-prop-Network"""
    #
    #class members
    #

    
    #
    # methods
    #
    def __init__(self, layerSize, weightStrad=np.random.uniform, transF=transs.sgm,wMin=1,  enableBias=True):
        '''
        @param layerSize: Tupel of the Layer Size, at least two integers. e.g. (16,5,4)
        @param weightStrad: The weight initialization strategy. Needs a numpy.random.random-like input (numpy.random.random is used @ standard
        @param transF: The transfer function on a neural layer. See module transferFuncCollection.py
        @param wMin: the least weight normalization initialization parameter (0.1 to 0.5 suggested)
        @param enalbeBias: enables Bias nodes.
        '''
        """ init: @param: type(layerSize)=list/n-tupel"""
        self.layerCount = 0
        self.shape = None
        self.weights=[]
        self.layerCount = len(layerSize)-1
        self.shape = layerSize
        self.weightStrad = weightStrad
        #init input var
        self.input=0
        #input/out from last Run
        self._layerInput=[]
        self._layerOutput=[]
        self.lnCases=0
        self.actfunc=transF
        #weight arrays
        #The extra neuron fires always one na simulates the activation hill
        if enableBias:
            self.biasNode=1
        else:
            self.biasNode=0
        for(l1,l2)in zip(layerSize[:-1], layerSize[1:]):
            #zeros does not work
            #mine:
            self.weights.append(self.weightStrad((l2,l1+self.biasNode))*(np.sqrt(wMin/l2)))
            #original:
            #self.weights.append(np.random.normal(scale=0.01,size=(l2,l1+self.biasNode)))
    @obsub.event
    def set_NN_Error(self, arg):
        None
    #
    #run method
    #
    def Run(self, input_r):
        '''
        run NN
        '''
        self.input=input_r
        self.lnCases=self.input.shape[0]
        self._layerInput=[]
        self._layerOutput=[]
        for index in range(self.layerCount):
            #determine layerinput
            if index==0:
                layerInput=self.weights[0].dot(np.vstack([self.input.T, np.ones([self.biasNode,self.lnCases])]))
            else:
                layerInput=self.weights[index].dot(np.vstack([self._layerOutput[-1],np.ones([self.biasNode,self.lnCases])]))
            
            self._layerInput.append(layerInput)
            self._layerOutput.append(self.actfunc(layerInput))
        return self._layerOutput[-1].T
    
    
    def train(self, output_delta,trainingRate=0.2):
        '''
        des
        '''
        #first run NN
        #if not self.oneShot:
            #self.Run(input) #only in original needed, not in SARSA
        delta=[]
        #calc detas
        for index in reversed(range(self.layerCount)):
            if index==self.layerCount-1:
                #output error values values
                #original:
                #output_delta=self._layerOutput[index] - target.T 
                #//whereby instead of the error, the target is put as input in the training function
                #_mine: 
                #output_delta=target.T
                error=np.sum(output_delta**2)
                #mine:
                #one_resized=output_delta.reshape([output_delta.shape[0],1])
                #two=self.actfunc(self._layerInput[index], True)
                #original: 
                delta.append(np.multiply(output_delta, self.actfunc(self._layerInput[index],True)))
                #mine: 
                #delta.append(np.multiply(one_resized, two))
            else:
                #compare to following layers deltas
                delta_pullback=self.weights[index+1].T.dot(delta[-1])
                delta.append(np.multiply(delta_pullback[:-1,:],self.actfunc(self._layerInput[index],True)))
                
        #compute weight deltas
        for index in range(self.layerCount):
            delta_index=self.layerCount-1-index
            
            if index==0:
                #equals to layerOutput=self._layerInput[index] without the weight multiplication
                layerOuput=np.vstack([self.input.T, np.ones([self.biasNode,self.lnCases])])
            else:
                #still equals to layerOutput=self._layerInput[index] without the weight multiplication
                layerOuput=np.vstack([self._layerOutput[index-1],np.ones([self.biasNode,self.lnCases])])
            
            #This is from the original Ryan Harris implementation, which sums up all 
            #the weight-deltas from one epoche and does only one-time updat.
            #However this is not necessary in the sarsa algorithm, since we want to update the weights in each
            #training step (=epoche)
            weightDelta=np.sum(
                           layerOuput[None,:,:].transpose(2,0,1)*delta[delta_index][None,:,:].transpose(2,1,0)
                           ,axis=0)
            self.weights[index]-=trainingRate*weightDelta
            #print("weights: " , self.weights)
            #self.weights[index]=self.weights[index].clip(0,1)
        self.set_NN_Error(error)
        return error
        
class AmygdalaNN(NN):
    '''The Neural network with a separate fear learning structure '''
    def __init__(self, layerSize, weightStrad=np.random.ranf):
        super().__init__(layerSize, weightStrad=np.random.ranf, transF=transs.sgm ,wMin=1,  enableBias=True)
        '''initialize super with common parameters'''
        '''pre-learn a general fearful behavior.'''

        self.Amyg=NN(layerSize, weightStrad=np.random.ranf, transF=transs.tanghyper ,wMin=1,  enableBias=True)
        
        
        self.fear_experience=False
        
    @obsub.event
    def setError(self, arg):
        None
    
    def set_fear(self, arg, fear):
        '''@param fear: boolean, whether the agent encountered fear''' 
        self.fear_experience=fear
        None
    
    def train(self, output_delta,trainingRate=0.2):
        er_f=self.Amyg.train(output_delta[1],trainingRate) #delta times minus one? 
        er_r=NN.train(self, output_delta[0],trainingRate)
        self.setError((er_r, er_f))
        return np.absolute(er_r) +np.absolute(er_f)
        

        
        

    def Run(self, input_r):
        mlp_answer= NN.Run(self, input_r)
        amyg_answer=self.Amyg.Run(input_r)
        #fear_modifier=self.GenralFear.Run(input_r)
        '''this line suppresses the MLP outcome if the input state suggests danger'''
        return mlp_answer, amyg_answer

        
        
if __name__=='__main__':
    bpn = NN((2,3,1), weightStrad=np.random.random,transF=transs.sgm,wMin=5, enableBias=True)
    print (bpn.shape)
    print(bpn.weights)
    
    lvInput=np.array([[0.1,0.9],[0.9,0.9],[0.1,0.1],[0.9,0.1],[0.25,0.75],[0.75,0.25],[0.25,0.25],[0.75,0.75]])
    
    lvTarget=np.array([[0.02],[0.03],[0.04],[0.05],[0.9],[0.9],[0.9],[0.9]])
    
    lnMax=1000000
    lnErr=1e-4
    for i in range(lnMax-1):
        out= bpn.Run(lvInput)
        output_delta=out-lvTarget
        err=bpn.train(output_delta.T)
        if i%10000==0:
            print("Iteration{0}\tError: {1:0.6f}".format(i,err))
        if err<=lnErr:
            print("Minimum error reached at iteration {0}".format(i))
            break
            
    #disply
    lvOutput=bpn.Run(lvInput)
    print("Input:{0}nOuput: {1}".format(lvInput,lvOutput))