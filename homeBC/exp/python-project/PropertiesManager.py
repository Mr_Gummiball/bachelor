'''
Created on 05.11.2014

@author: Jonas
'''

import time
import unittest

import numpy as np
import transferFuncCollection as transs

class Properties(object):
    '''
    creates a parameter dictionary / DNA
    @param random: set True for random initialization
    @param paramDict: existing parameter dictionary, pass as python dict
    @param maxInt: boundary for random initialization
    @param maxDeaths: dummy parameter for maximum amount of deaths, currently not used.
    ''' 
    def __init__(self, random=False, paramDict=None, maxInt=10000, maxDeaths=5):

        self.DNA=['runs', 'weightInitMin', 'worldSize', 'maxDeath', 'transferFunc', 'discountFactor', 'reliability', 'learningRate', 'fitness', 'reward', 'weightInit', 'punishment', 'momentum', 'hiddenLayerSize', 'danger']
        self.maxInt=maxInt
        self.meta={"hiddenLayerSize":int, "weightInitMin":int,
                   "reward":int, "reliability":int,
                   "punishment":"int_neg", "transferFunc":bool,
                   "discountFactor":float, "learningRate":float,
                   "momentum": float, "fitness":int}
        '''initialization with given parameters'''
        self.paramDict={}
        if paramDict:
            for acid in self.DNA:
                self.paramDict.update({acid:paramDict.get(acid)})
        #initialization of new parameters
        else:
            #fixed parameters
            self.paramDict={"maxDeath":maxDeaths,"runs":600000,"weightInit":np.random.ranf}
            self.paramDict.update({"worldSize":(8,4), "fitness":999, "danger":True})
            #choose fixed setup
            if random==False:
                self.paramDict.update({"discountFactor":0.9, "hiddenLayerSize":10, 
                          "weightInitMin":1,
                           "reward":4, "punishment":-2,
                          "learningRate":0.1, "reliability":8.1146, "momentum":1})
                self.paramDict.update({"transferFunc":transs.sgm})
            #random individual creation
            else:
                self.paramDict.update({"discountFactor":np.random.random(), "hiddenLayerSize":np.random.randint(2, high=100), 
                          "weightInitMin":np.random.randint(low=1, high=100), "transferFunc":self.decideTransF(np.random.random()>0.5),
                           "reward":np.random.randint(1,high=10), "punishment":np.random.randint(1, high=500)*-1,
                          "learningRate":np.random.random(), "reliability":np.random.randint(0, high=2000)/50,
                          "momentum": np.random.random()})
        
        self.definition=self.define()
        self.uniqueCode=time.time()
        
        
    def createRandom(self, item, max=None):
        assert item, "trying to flip a none existing parameter"
        if not max:
            max=self.maxInt
        if item==int:
            return np.random.randint(max)
        elif item==float:
            return np.random.random()
        elif item=="int_neg":
            return np.random.randint(max)*-1
        else:
            print ("parameter meta undefined: ", item)
        
            
    def flipp(self, hard=True):
        '''mutates the specified parameters'''
        rand=np.random.randint(len(self.meta)-1)
        param = list(self.meta)[rand]
        if param=="transferFunc":
            self.paramDict.update({"transferFunc":self.decideTransF(np.random.random()>0.5)})
        else:
            if not hard:
                max=100
                value=self.createRandom(self.meta.get(param),max=max)
                self.paramDict.update({param:value})
            else:
                value=self.createRandom(self.meta.get(param))
                self.paramDict.update({param:value})

    def getFitness(self):
        return self.paramDict.get("fitness")                    
    def define(self):
        '''
        creates a TODO: wort einfuegen
        '''
        definition =int(0)
        for paramDescription in self.paramDict:
            definition*= int(ord(list(paramDescription)[0]))
        return definition
    
    def createRandomList(self, many):
        '''returns a random list of items of many in max(len(self.meta))'''
        max=len(set(self.meta))-1
        assert  max>=many, "to many values selected for child merger"
        return np.random.randint(0,high=max, size=many)
            
    
    def merge(self, propObject):
        '''merge with another properties object'''
        assert self.definition ==propObject.define() , "two different property-definitions"
        bDNA=self.createRandomList(len(set(self.meta))-1)
        bParams=propObject.getReflection()
        for item in bDNA.astype(list):
            param=list(set(bParams))[item]
            bValue=bParams.get(param)
            self.paramDict.update({param:bValue})
            
        
    def getReflection(self):
        '''return properties object as reflection'''
        return self.paramDict
    
    
    def decideTransF(self, x):
        assert type(x)==bool, "only boolean allowed to switch for transferfunction"
        if x:
            return transs.tanghyper
        else:
            return transs.sgm
    
    def createWorldShape(self, x):
        #TODO:kinda unbug, emtpy meaning
        return (14,7)
    
    def getCopy(self):
        return Properties( random=False,paramDict=self.getReflection())
    
    def __str__(self):
        print("number: ", self.uniqueCode,". " ,self.paramDict)

    def __str__2(self):
        print(str(self.uniqueCode))

    def checkJson_able(self, tC):
        '''
        white lists types that can be transformed to bson without problems.
        '''
        if type(tC)==float:
            return True
        elif  type(tC)==int:
            return True
        elif  type(tC)==list:
            return True
        elif  type(tC)==str:
            return True
        elif  type(tC)==bool:
            return True
        #for tupel
        elif  type(tC)==type((1233, 123)):
            return True
        else:
            return False
        
    def to_json(self):
        jSon_dict=self.paramDict
        for desc in self.paramDict:
            if not self.checkJson_able(self.paramDict.get(desc)):
                jSon_dict.update({desc:self.paramDict.get(desc).__name__})
        return jSon_dict.copy()
    
    def from_json(self):
        if "sgm" in self.paramDict.get("transferFunc"):
            self.paramDict.update({"transferFunc":transs.sgm})
        elif "tanghyper" in self.paramDict.get("transferFunc"):
            self.paramDict.update({"transferFunc":transs.tanghyper})
        else:
            print ("WARNING undefined Transferfunction @ load")
        if "random_sample" in self.paramDict.get("weightInit"):
            self.paramDict.update({"weightInit":np.random.random})
        
            

class Tester(unittest.TestCase):
    def setUp(self):
        self.x=np.random.random()>0.5
    def test_create(self):
        self.props=Properties()
        
if __name__=="__main__":
    for i in range(100):
        unittest.main()