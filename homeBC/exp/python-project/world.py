'''
Created on 17.10.2013

@author: dasin_000
@author: danger world: Jonas Sperling
y and x swapped
'''
import obsub
import numpy as np


class world(object):
    '''
    classdocs
    '''

    def __init__(self, maxY, maxX):
        '''
        Constructor
        @param maxY: Y-axe size
        @param maxX: x-axe size
        '''
        
        super().__init__()
        self.max_grid_x = maxX
        self.max_grid_y = maxY
        self.max_reward = 1
        self.non_reward=0
        self.target_y = round(maxY/2)
        self.target_x = round(maxX/2)

        
    
    def zeroGrid(self):
        return np.zeros(shape=(self.max_grid_x+3, self.max_grid_y+3))
    @obsub.event
    def pos(self, arg):
        None
        
    @obsub.event
    def initPos(self, arg):
        None
        
    @obsub.event
    def deathTick(self, arg):
        None  
        
    @obsub.event
    def targetTick(self):
        None       

    
   
    def initialise(self):
        ''' initialise grid values of max_grid_x times max_grid_x and set random position '''
        self.pos_x = np.random.randint(1, self.max_grid_x-1)
        self.pos_y = np.random.randint(1, self.max_grid_y-1)
        while  self.pos_x==self.target_x and self.pos_y==self.target_y:
            self.pos_x = np.random.randint(1, self.max_grid_x-1)
            self.pos_y = np.random.randint(1, self.max_grid_y-1)
        target=self.zeroGrid()
        target[self.target_x+1, self.target_y+1]=1
        pos=self.zeroGrid()
        pos[self.pos_x+1, self.pos_y+1]=1
        self.initPos(target)
        self.pos(pos)
        #debug:
        self.zaehler=0
        
    
    def action(self, h):
        ''' act perform one action to move sensor
        0: up
        1: right
        2: down
        3: left
        
        @param act: action as integer between 0 and 3 
        '''
        act = np.nonzero(h)[0]
        #reference: http://docs.scipy.org/doc/np/reference/generated/np.nonzero.html
        if act == 0:
            # down
            if (self.pos_y == 0):
                self.pos_y = 0
            else:
                self.pos_y -= 1
            
        elif act == 1:
            # right
            if (self.pos_x == self.max_grid_x):
                self.pos_x = self.max_grid_x
            else:    
                self.pos_x += 1
        elif act == 2:
            # up
            if (self.pos_y == self.max_grid_y):
                self.pos_y =self.max_grid_y
            else:
                self.pos_y += 1
        elif act == 3:
            # left
            if (self.pos_x ==0):
                self.pos_x = 0
            else:
                self.pos_x -= 1
        elif act > 3:
            print("Index out of bounds! Only four actions idiot.") 
            print (act)
        
        pos=self.zeroGrid()
        pos[self.pos_x+1, self.pos_y+1]=1
        self.pos(pos)
        self.zaehler+=1
    

    
    def getState(self, flat=False):
        if flat:
            return self.getStateAsFlattenMatrix()
        else:
            result  = np.zeros(2)
            result[0] = self.pos_x /(self.max_grid_x)
            result[1] = self.pos_y/ (self.max_grid_y)
            return result

    def getStateAsFlattenMatrix(self):
        result = np.zeros(shape = (self.max_grid_x+1,self.max_grid_y+1))
        result[self.pos_x][self.pos_y]= 1
        result = np.resize(result, len(result.flatten()))
        return result
               
        
    def getReward(self):
        ''' returns the reward '1' if position is reached 
        @return 0 or 1'''
        if self.pos_y == self.target_y and self.pos_x == self.target_x:
            self.targetTick()
            self.zaehler=0
            return self.posReward()
        else:
            answer= self.non_reward
            self.non_reward=0
            return answer     
                 
    def posReward(self):
        return self.max_reward
    

class dangerWorld(world):
    '''inherited of world to add danger'''
    def __init__(self, maxY, maxX, reward, punishment, die=True):
        super().__init__(maxY, maxX)
        self.max_reward=reward
        self.punishment=punishment
        self.die=die
        self.deathMap=self.zeroGrid()
        
    def __str__(self):
        return 'dangerWorld'

    def action(self, h):
        ''' act perform one action to move sensor
        0: up
        1: right
        2: down
        3: left
        
        @param act: action as integer between 0 and 3 
        '''
        act = np.nonzero(h)[0]
        #reference: http://docs.scipy.org/doc/np/reference/generated/np.nonzero.html
        if act == 0:
            if (self.pos_y == 0):
                self.pos_y = 0
                self.non_reward=self.punishment
                self.deathMap[self.pos_x+1, self.pos_y]+=1
                self.deathTick(self.deathMap)
                if self.die:
                    self.initialise()
            else:
                self.pos_y -= 1
            
        elif act == 1:
            # right
            if (self.pos_x == self.max_grid_x):
                self.pos_x = self.max_grid_x
                #self.non_reward=self.punishment
                self.non_reward=0
                #self.deathMap[self.pos_x+1, self.pos_y+1]+=1
                #self.deathTick(self.deathMap)
                #if self.die:
                #    self.initialise()
            else:    
                self.pos_x += 1
                
        elif act == 2:
            # up
            if (self.pos_y == self.max_grid_y):
                self.pos_y =self.max_grid_y
                #self.non_reward=self.punishment
                self.non_reward=0
                #self.deathMap[self.pos_x+1, self.pos_y+2]+=1
                #self.deathTick(self.deathMap)
                #if self.die:
                #    self.initialise()
            else:
                self.pos_y += 1
        elif act == 3:
            # left
            if (self.pos_x ==0):
                self.pos_x = 0
                self.non_reward=0
                #self.deathMap[self.pos_x, self.pos_y+1]+=1
                #self.deathTick(self.deathMap)
                #if self.die:
                #    self.initialise()
            else:
                self.pos_x -= 1
        elif act > 3:
            print("Index out of bounds.") 
            print (act)
        pos=self.zeroGrid()
        pos[self.pos_x+1, self.pos_y+1]=1
        self.pos(pos)
        self.zaehler+=1



        
    

    