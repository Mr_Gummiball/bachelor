'''
result_analysis.gradient -- Draw a Gradient Map



@author:     Jonas Sperling


@contact:    1sperlin@informatik.uni-hamburg.de
'''



from argparse import ArgumentParser
import sarsa
import PropertiesManager as PM
import BackProp as BP
import numpy as N
from matplotlib import pyplot as plt
import matplotlib as mpl



def printGradientMap(danger=False, algo=sarsa.table,toSave=None):

    par=PM.Properties(random=False)
    par.paramDict['danger']=danger
    sa=algo(par.paramDict)
    print("fitness=" ,sa.run())
    target=sa.interface.target_y ,sa.interface.target_x
    if algo==sarsa.Separated:
        #separate weights
        init_head=sa.layerSize
        reward_circuit=BP.NN(init_head)
        reward_circuit.weights=sa.wc.weights
        fear_circuit=BP.NN(init_head)
        fear_circuit.weights=sa.wc.Amyg.weights
        #run separated gradient
        mapper=GradientMaps('Gradient_reward')
        mapper.rageOfState(reward_circuit, sa.interface)
        mapper.showMap(desc=str(target), savePlace=toSave+'_reward_circuit.png')
        mapper=GradientMaps('Gradient_fear')
        mapper.rageOfState(fear_circuit, sa.interface)
        mapper.showMap(desc=str(target),savePlace=toSave+'_fear_circuit.png')
    else:
        mapper=GradientMaps('Gradient')
        mapper.rageOfState(sa.wc, sa.interface)
        mapper.showMap(desc=str(target),savePlace=toSave+'.png')
            
class GradientMaps(object):
    '''Observer of sarsa'''
    def __init__(self,name):
        self.name = name
        self.gradientMap_s=None
        self.gradientMap_e=None
        self.gradientMap_n=None
        self.gradientMap_w=None
        self.discount=0.9
        '''for every NN output one map'''
    
    def rageOfState(self, nn, world):
        flat=True
        if len(nn.weights)>1:
            flat=False

        for i in range(0, world.max_grid_x):
            for j in range(0, world.max_grid_y):
                world.pos_x=i
                world.pos_y=j
                state=world.getState(flat=flat)
                nn_out=nn.Run(N.resize(state, (1,len(state))))
                sValues=N.resize(nn_out, (nn_out.shape[1]))
                arg=0
                self.listenToValues(arg,(sValues, (world.max_grid_x+1, world.max_grid_y+1),world.getState(flat=True)))
    
    def showMap(self,desc='none',savePlace=None):
        mpl.use('agg')
        from mpl_toolkits.axes_grid1.inset_locator import inset_axes
        import matplotlib.gridspec as gridspec
        len_x=self.gradientMap_s.shape[0]-1
        len_y=self.gradientMap_s.shape[1]-2
        slice_=[slice(0,len_x),slice(0,len_y+1)]   
        #print("south",N.round(self.gradientMap_s[slice_], 2))
        #print("east", N.round(self.gradientMap_e[slice_], 2))
        #print("north" , N.round(self.gradientMap_n[slice_], 2))
        #print("west", N.round(self.gradientMap_w[slice_], 2))
        ##normalize
        from sklearn import preprocessing
        normalizer = preprocessing.Normalizer().fit(self.gradientMap_s[slice_])
        self.gradientMap_sn=self.gradientMap_s[slice_]
        self.gradientMap_en=self.gradientMap_e[slice_]
        self.gradientMap_nn=self.gradientMap_n[slice_]
        self.gradientMap_wn=self.gradientMap_w[slice_]
        '''figure'''
        fig = plt.figure(figsize=(32, 16))
        gs1 = gridspec.GridSpec(1, 5)
        ax1 = fig.add_subplot(gs1[0])
        ax2 = fig.add_subplot(gs1[1])
        ax3 = fig.add_subplot(gs1[2])
        ax4 = fig.add_subplot(gs1[3])
        #fig, (ax1, ax2, ax3, ax4) = plt.subplots(1,4)
        plt.suptitle(' The gradient q-values for each action. Target at: '+desc+ " "+str(self.name))
        
    
        ax1.set_title('Action 0')
        im1 = ax1.imshow(self.gradientMap_sn,interpolation='nearest',origin='image')
        
        ax2.set_title('Action 1')
        im2 = ax2.imshow(self.gradientMap_en,interpolation='nearest',origin='image')
    
        ax3.set_title('Action 2')
        im3 = ax3.imshow(self.gradientMap_nn,interpolation='nearest',origin='image')
    
        ax4.set_title('Action 3')
        im4 = ax4.imshow(self.gradientMap_wn,interpolation='nearest',origin='image')
        #ax5 = fig.add_subplot(gs1[4])# ,aspect='equal')
        
        
        #cbar = fig.colorbar(im1,  orientation='vertical',cax = ax5)#, shrink=.5, pad=.2, aspect=10, cax = ax5)
        #colorbar
        axins = inset_axes(ax4,
                           width="15%", # width = 10% of parent_bbox width
                           height="100%", # height : 50%
                           loc=3,
                           bbox_to_anchor=(1.5, 0., 1, 1),
                           bbox_transform=ax4.transAxes,
                           borderpad=0,
                           )
        plt.colorbar(im4, cax=axins)#,ticks=[1,2,3])
        
        # Make space for title
        #origin=upper seems wrong,
        
        gs1.tight_layout(fig)
        #fig2, (ax5) = plt.subplots(1,1)
        #ax5.set_title('cumulative gradient descent')
        #pic=(self.gradientMap_w+self.gradientMap_s+self.gradientMap_e+self.gradientMap_n)[slice_]
        #im5 = ax5.imshow(pic,interpolation='nearest',origin='image')
        #cbar2 = fig2.colorbar(im5,  orientation='vertical')
        if savePlace:
            fig.savefig(savePlace, bbox_inches='tight',dpi=300)
        #plt.show()
           
    def listenToValues(self,arg, msg):
        '''output, shape (tupel),input'''
        sValue=msg[0]
        old_shape=(msg[1])
        
        x=N.resize(msg[2],old_shape).nonzero()[0][0]
        y=N.resize(msg[2],old_shape).nonzero()[1][0]
        #print("coordinate filler point: (",x,y,") is ", sValue)
        if self.gradientMap_s is None:
            
            #(pos[0]=x, pos[1]=y)
            #@world 0 equals down / south
            shape_s=(old_shape[0],old_shape[1])
            self.gradientMap_s=N.zeros(shape_s)
            self.gradientMap_s[x,y]=sValue[0]
            #@world 1 equals right / east
            self.gradientMap_e=N.zeros(shape_s)
            self.gradientMap_e[x,y]=sValue[1]
            #@world 2 equals up, north                    
            self.gradientMap_n=N.zeros(shape_s)
            self.gradientMap_n[x,y]=sValue[2]
            #@world 3 equals left, west           
            self.gradientMap_w=N.zeros(shape_s)
            self.gradientMap_w[x,y]=sValue[3]
        else:
            self.gradientMap_s[x,y]+=sValue[0]
            self.gradientMap_e[x,y]+=sValue[1]
            self.gradientMap_n[x,y]+=sValue[2]
            self.gradientMap_w[x,y]+=sValue[3]
            
            




if __name__ == "__main__":
    '''Command line options.'''



    # Setup argument parser
    parser = ArgumentParser(description='-h for possible arguments')
    parser.add_argument("-e", dest="experiment", help="Which Experiment Do You want to start? \"mlP\" or \"talbe\" or separated",
            type=str)
    parser.add_argument("-d", dest="danger",help="set the dangerous environment (to 1)", type=int)
    parser.add_argument("-s", dest="savePlace",help="the path and name You want to save the gradient map (without ending), file is saved as png", type=str)
    
    # Process arguments
    args = parser.parse_args()
    print(args.experiment,args.danger ,args.savePlace)
    algoType=args.experiment
    if "table" in algoType.lower():
        algo=sarsa.table
    elif "mlp" in algoType.lower():
        algo=sarsa.MLP
    elif "separated" in algoType.lower():
        algo=sarsa.Separated
    else:
        raise LookupError("No valid experiment found. Your choices are: \"table\" \"MLP\" and  \"separated\".")
    
    if args.danger==1:
        danger=True
    else:
        danger=False
    
    printGradientMap(danger=danger, algo=algo, toSave=args.savePlace)