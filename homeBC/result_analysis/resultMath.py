'''created jan 2015 by jonas'''
from matplotlib import pyplot as plt
import pandas
from pandas.core.series import Series
from pandas.tools.plotting import autocorrelation_plot
from pandas.tools.plotting import bootstrap_plot
import pymongo

import BackProp as BP
import PropertiesManager as PM
import matplotlib as mpl
import numpy as N
import pylab as P
import sarsa
import transferFuncCollection as TF


def timeCorrs(series):
    
    print("Autocorrelations")
    plt.figure()
    autocorrelation_plot(series)
    plt.show()


class MLPerror_Vis(object):

    def __init__(self, danger=False, algo=sarsa.MLP):
        '''
        @param danger: if True, agent encounters death upon rim.
        @param algo: sarsa algorithm object
        '''
        self.error_tripple_list=[]
        self.oneError=[]
        self.deathTiccc=[]
        self.deathLine=[]
        par=PM.Properties(random=False)
        par.paramDict['danger']=danger
        self.sa=algo(par.paramDict)
        
    def printSimpleLine(self, times):
        self.sa.toLearnTerm+=self.listenToSimpleError
        print("fitness", self.sa.run())
        fig=plt.plot()
        plt.suptitle('cumulative Error plot')
        x_list=[]
        y_list=[]
        for i in range(0,int((len(self.oneError)-times)/times)):
            x_list.append(i)
            y_list.append(N.sum(self.oneError[i*times:(i+1)*times]))
        plt.errorbar(x_list, y_list)
        plt.show()
    
    def printSimpleLineWithDeath(self, times):

        self.sa.toLearnTerm+=self.listenToSimpleError
        self.sa.punish+=self.listenToDeath
        print("fitness", self.sa.run())
        
        x_list=[]
        y_list=[]
        y_death_list=[]
        for i in range(0,int((len(self.oneError)-times)/times)):
            x_list.append(i)
            y_list.append(N.absolute(N.sum(self.oneError[i*times:(i+1)*times])))
            y_death_list.append(N.sum(self.deathLine[i*times:(i+1)*times]))
        x=N.array(x_list)
        y_er=N.array(y_list)    
        y_death=N.array(y_death_list)
        
    
        from scipy.optimize import curve_fit
        
        def func(x, a, b,c):
            return a * N.exp(-b * (1+x)) +c
        
        
        popt_er, pcov = curve_fit(func, x, y_er)
        popt_d, pcov_d = curve_fit(func, x, y_death)
        
        fig, ax1 = plt.subplots()
        plt.suptitle(self.sa.__str__()[1:15])
        
        ax1.plot(x, y_er, 'g.',x, func(x, *popt_er), 'b-', label="Fitted error Curve") 
        
        ax1.set_xlabel('cumulative run over '+ str(times)+  ' times.')
        ax1.set_ylabel('output delta prediction', color='b')
        
        ax2 = ax1.twinx()
        ax1.get_shared_y_axes().join(ax1, ax2)
        
        ax2.plot(x, y_death, 'yx', x, func(x, *popt_d), 'r-', label="Fitted death Curve")
        ax2.set_ylabel('cumulative times of death prediction', color='r')
        plt.show()
        
    def printSeparatedLine(self, sa=None, tick_length=None):
        if sa:
            pass
        else:
            sa=self.sa
        if tick_length:
            pass
        else:
            tick_length=int(sa.runs/100)
        sa.wc.setError+=self.listenToError
        sa.punish+=self.listenToDeath
        print("fitness=" ,sa.run())
        if (len(self.error_tripple_list)>0):
            mlp_err=N.array([sum(self.error_tripple_list[x:x+tick_length:][0]) for x in range(int(len(self.error_tripple_list)/tick_length))])
            fear_err=N.array([sum(self.error_tripple_list[y:y+tick_length:][1]) for y in range(int(len(self.error_tripple_list)/tick_length))])
            
            death_Err=N.array([sum(self.deathTiccc[z:z+tick_length:]) for z in range(int(len(self.deathTiccc)/tick_length))])
            
            dx_list=[]
            iter=0
            print("error-shape", mlp_err.shape)
            
            x_list=[]
            for i in range(len(mlp_err)):
                x_list.append(iter)
                iter+=1
            
            iter_m=0
            for y in range(len(death_Err)):
                dx_list.append(iter_m)
                iter_m+=1
            fig, (ax1, ax2, ax3) = plt.subplots(1,3)
            plt.suptitle('Error plots')
    
            ax1.set_title('square error of the MLP')
            im1 = ax1.errorbar(x_list, mlp_err)
            
            ax2.set_title('fear error')
            im2 = ax2.errorbar(x_list, fear_err)
        
            ax3.set_title('death ticks')
            im3 = ax3.errorbar(dx_list, death_Err)
            print("deathline:", death_Err)
        else:
            plt.figure()
            plt.errorbar(x_list, mlp_err)#, 'r', x_list, fear_err, 'b', x_list, preLearned_err, 'g')
        plt.show()
        
    def listenToError(self,arg, er_duo):
        mlp_err=N.sum(N.absolute(er_duo[0]))
        amyg_err=N.sum(N.absolute(er_duo[1]))
        self.error_tripple_list.append((mlp_err,amyg_err))

    def listenToSimpleError(self,arg, error):
        mlp_err=N.sum(error)
        self.deathLine.append(N.sum(self.deathTiccc))
        self.deathTiccc.clear()
        self.oneError.append(mlp_err)    
    def listenToDeath(self, arg, deathBool):
        if deathBool:
            self.deathTiccc.append(1)
        else:
            self.deathTiccc.append(0)
    

    
def prepareErrorTimeLine(paramList, algolist,runs=10):
    '''
    @param paramListas dicts for the PropertiesManager
    @return :list of pandas Series  
    ''' 
    '''lets let the top n run for 10runs and see their fitness'''
    assert len(paramList)==len(algolist), "length of parameter and algos have to be the same"
    mcl=pymongo.MongoClient()
    db=mcl.Client['err_line']
    for (par,algo) in zip(paramList, algolist):
        ser_list=[]
        for i in range(0,runs-1):
            indi=PM.Properties(paramDict=par)
            indi.from_json()
            sa=algo(indi.paramDict)
            ser_list.append(sa.run())
        _id=db.insert({sa.__name__:ser_list})
        db.insert({sa.__name__:par.to_json()})
        print("inserted:", _id)
        
        
        

    
def prepareFitenssTimeLine(paramList, runs=10):
    '''
    @param paramListas dicts for the PropertiesManager
    @return :list of pandas Series  
    ''' 

    '''lets let the top 5 run for 10runs and see their fitnesses'''
    
    mcl=pymongo.MongoClient('localhost', 27017)
    db=mcl.Client['fit_line']
       
    for k in paramList:
        ser_list=[]
        for i in range(0,runs-1):
            indi=PM.Properties(paramDict=k)
            indi.from_json()
            sa=sarsa.MLP(indi.paramDict, showcase=False, visualizeMovement=False)
            ser_list.append(sa.run())
        _id=db.insert({'fitness':ser_list})
        print("inserted:", _id)
        
        
def analyseFitLine():
    
    mcl=pymongo.MongoClient('localhost', 27017)
    db=mcl.Client['fit_line']
    alive_population=db.find({"fitness":{"$lt":1000}})
    pa=pandas.DataFrame(list(alive_population))
    max=pa.index.max()
    if pa.index.max()>5:
        max=5
    for i in range(0, max):
        curSer=Series(pa.loc[i][1])
        if curSer.shape[0]>500:
            timeCorrs(curSer)
            bootstrap_plot(curSer, size=50, samples=500, color='grey')
            plt.show()

 
def deathDescent():
    Deathmapper=GradientMaps('Gradient')
    par=PM.Properties(random=False)
    par.paramDict['danger']=True
    sa=sarsa.Separated(par.paramDict)
    sa.q_values += Deathmapper.listenToValues
    print("fitness=" ,sa.run())   
              
def printGradientMap(par=PM.Properties(random=False), algo=sarsa.table,toSave=None):
    
    sa=algo(par.paramDict)
    print("fitness=" ,sa.run())
    target=sa.interface.target_y ,sa.interface.target_x
    if algo==sarsa.Separated:
        #separate weights
        init_head=sa.layerSize
        reward_circuit=BP.NN(init_head)
        reward_circuit.weights=sa.wc.weights
        fear_circuit=BP.NN(init_head)
        fear_circuit.weights=sa.wc.Amyg.weights
        #run separated gradient
        mapper=GradientMaps('Gradient_reward')
        mapper.rageOfState(reward_circuit, sa.interface)
        mapper.showMap(desc=str(target), savePlace=toSave+'_reward_circuit.png')
        mapper=GradientMaps('Gradient_fear')
        mapper.rageOfState(fear_circuit, sa.interface)
        mapper.showMap(desc=str(target),savePlace=toSave+'_fear_circuit.png')
    else:
        mapper=GradientMaps('Gradient')
        mapper.rageOfState(sa.wc, sa.interface)
        mapper.showMap(desc=str(target),savePlace=toSave+'.png')
            
class GradientMaps(object):
    '''Observer of sarsa'''
    def __init__(self,name):
        self.name = name
        self.gradientMap_s=None
        self.gradientMap_e=None
        self.gradientMap_n=None
        self.gradientMap_w=None
        self.discount=0.9
        '''for every NN output one map'''
    
    def rageOfState(self, nn, world):
        flat=True
        if len(nn.weights)>1:
            flat=False

        for i in range(0, world.max_grid_x):
            for j in range(0, world.max_grid_y):
                world.pos_x=i
                world.pos_y=j
                state=world.getState(flat=flat)
                nn_out=nn.Run(N.resize(state, (1,len(state))))
                sValues=N.resize(nn_out, (nn_out.shape[1]))
                arg=0
                self.listenToValues(arg,(sValues, (world.max_grid_x+1, world.max_grid_y+1),world.getState(flat=True)))
    
    def showMap(self,desc='none',savePlace=None):
        
        from mpl_toolkits.axes_grid1.inset_locator import inset_axes
        import matplotlib.gridspec as gridspec
        len_x=self.gradientMap_s.shape[0]-1
        len_y=self.gradientMap_s.shape[1]-2
        slice_=[slice(0,len_x),slice(0,len_y+1)]   
        #print("south",N.round(self.gradientMap_s[slice_], 2))
        #print("east", N.round(self.gradientMap_e[slice_], 2))
        #print("north" , N.round(self.gradientMap_n[slice_], 2))
        #print("west", N.round(self.gradientMap_w[slice_], 2))
        ##normalize
        from sklearn import preprocessing
        normalizer = preprocessing.Normalizer().fit(self.gradientMap_s[slice_])
        self.gradientMap_sn=self.gradientMap_s[slice_]
        self.gradientMap_en=self.gradientMap_e[slice_]
        self.gradientMap_nn=self.gradientMap_n[slice_]
        self.gradientMap_wn=self.gradientMap_w[slice_]
        '''figure'''
        fig = plt.figure(figsize=(32, 16))
        gs1 = gridspec.GridSpec(1, 5)
        ax1 = fig.add_subplot(gs1[0])
        ax2 = fig.add_subplot(gs1[1])
        ax3 = fig.add_subplot(gs1[2])
        ax4 = fig.add_subplot(gs1[3])
        #fig, (ax1, ax2, ax3, ax4) = plt.subplots(1,4)
        plt.suptitle(' The gradient q-values for each action. Target at: '+desc+ " "+str(self.name))
        
    
        ax1.set_title('Action 0')
        im1 = ax1.imshow(self.gradientMap_sn,interpolation='nearest',origin='image')
        
        ax2.set_title('Action 1')
        im2 = ax2.imshow(self.gradientMap_en,interpolation='nearest',origin='image')
    
        ax3.set_title('Action 2')
        im3 = ax3.imshow(self.gradientMap_nn,interpolation='nearest',origin='image')
    
        ax4.set_title('Action 3')
        im4 = ax4.imshow(self.gradientMap_wn,interpolation='nearest',origin='image')
        #ax5 = fig.add_subplot(gs1[4])# ,aspect='equal')
        
        
        #cbar = fig.colorbar(im1,  orientation='vertical',cax = ax5)#, shrink=.5, pad=.2, aspect=10, cax = ax5)
        #colorbar
        axins = inset_axes(ax4,
                           width="15%", # width = 10% of parent_bbox width
                           height="100%", # height : 50%
                           loc=3,
                           bbox_to_anchor=(1.5, 0., 1, 1),
                           bbox_transform=ax4.transAxes,
                           borderpad=0,
                           )
        plt.colorbar(im4, cax=axins)#,ticks=[1,2,3])
        
        # Make space for title
        #origin=upper seems wrong,
        
        #gs1.tight_layout(fig)
        #fig2, (ax5) = plt.subplots(1,1)
        #ax5.set_title('cumulative gradient descent')
        #pic=(self.gradientMap_w+self.gradientMap_s+self.gradientMap_e+self.gradientMap_n)[slice_]
        #im5 = ax5.imshow(pic,interpolation='nearest',origin='image')
        #cbar2 = fig2.colorbar(im5,  orientation='vertical')
        if savePlace:
            fig.savefig(savePlace, bbox_inches='tight',dpi=300)
        #plt.show()
           
    def listenToValues(self,arg, msg):
        '''output, shape (tupel),input'''
        sValue=msg[0]
        old_shape=(msg[1])
        
        x=N.resize(msg[2],old_shape).nonzero()[0][0]
        y=N.resize(msg[2],old_shape).nonzero()[1][0]
        #print("coordinate filler point: (",x,y,") is ", sValue)
        if self.gradientMap_s is None:
            
            #(pos[0]=x, pos[1]=y)
            #@world 0 equals down / south
            shape_s=(old_shape[0],old_shape[1])
            self.gradientMap_s=N.zeros(shape_s)
            self.gradientMap_s[x,y]=sValue[0]
            #@world 1 equals right / east
            self.gradientMap_e=N.zeros(shape_s)
            self.gradientMap_e[x,y]=sValue[1]
            #@world 2 equals up, north                    
            self.gradientMap_n=N.zeros(shape_s)
            self.gradientMap_n[x,y]=sValue[2]
            #@world 3 equals left, west           
            self.gradientMap_w=N.zeros(shape_s)
            self.gradientMap_w[x,y]=sValue[3]
        else:
            self.gradientMap_s[x,y]+=sValue[0]
            self.gradientMap_e[x,y]+=sValue[1]
            self.gradientMap_n[x,y]+=sValue[2]
            self.gradientMap_w[x,y]+=sValue[3]
            
            



    

if __name__=='__main__':
    
    printGradientMap(algo=sarsa.Separated, toSave='/home/jonas/Pictures/verteidigung/Separated')
    #printGradientMap(danger=True, algo=sarsa.table, toSave='/home/jonas/Pictures/table_fear')
    #printGradientMap(danger=True, algo=sarsa.Separated, toSave='/home/jonas/Pictures/Separated')
    #runVisSarsa()
    #er_obj=MLPerror_Vis(danger=True, algo=sarsa.Separated)
    #er_obj.sa.runs=200000
    #er_obj.printSeparatedLine()
    #multitask_appendTestparameters(punish_list=[-0.84619,-1.0134,-2.675])
    #appendTestparameters(punishment=-0.01)
    
'''
if __name__=='__main__':
    parser = argparse.ArgumentParser(description='arguments: Exp-Number (1 or 2, host for db, port for db. Integer values separated by space')
    parser.add_argument("experiment", help="Which Experiment Do You want to start? \"MlP\" or \"talbe\"",
                type=str)
    parser.add_argument("method", help= "1=prepareErrorTimeLine or 0= ....",
                type=int)
    parser.add_argument("algoType ", help="1=MLP, 0=table",
                type=int)
    parser.add_argument("limit", help="enter int, number of individuals to keep",
                type=int)
    args = parser.parse_args()
    if args.algoType==0:
        MLP=sarsa.table
    else:
        MLP=sarsa.MLP

    mcl=MongoClient('localhost', 27017)
    db=mcl.Client[args.experiment]
    alive_population=db.find({"fitness":{"$lt":1000}})
    limited=alive_population.limit(args.limit)
    print("number of obervations limited to: ", limited.explain().get("n"))
    if args.method==1:
        prepareErrorTimeLine(list(limited), runs=2, MLP)
    else:
    #sorted=alive_population.sort("fitness", pymongo.ASCENDING)
        prepareFitenssTimeLine(list(limited), runs=1000)
        
        
import resultMath as RM
import sarsa
import PropertiesManager as PM
par=PM.Properties(random=False)
par.paramDict['danger']=True
param_list=[]
param_list.append(par)
param_list.append(par.getReflection().update({"punishment":-1}))
param_list.append(par.getReflection().update({"punishment":-10}))
sarsa_list=[]
sarsa_list.append(sarsa.MLP)
sarsa_list.append(sarsa.table)
sarsa_list.append(sarsa.Separated)
sarsa_list=sarsa_list*3
param_list=param_list*3
'''