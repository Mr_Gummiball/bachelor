'''created jan 2015 by jonas'''
from matplotlib import pyplot as plt
import matplotlib as mpl

import pandas
from pandas.core.series import Series
from pandas.tools.plotting import autocorrelation_plot
from pandas.tools.plotting import bootstrap_plot
import pymongo

import BackProp as BP
import PropertiesManager as PM
import matplotlib as mpl
import numpy as N
import pylab as P
import sarsa
import transferFuncCollection as TF

def _blob(x,y,area,colour):
    """
    reference: http://wiki.scipy.org/Cookbook/Matplotlib/HintonDiagrams
    Draws a square-shaped blob with the given area (< 1) at
    the given coordinates.
    """
    hs = N.sqrt(area) / 2
    xcorners = N.array([x - hs, x + hs, x + hs, x - hs])
    ycorners = N.array([y - hs, y - hs, y + hs, y + hs])
    P.fill(xcorners, ycorners, colour, edgecolor=colour)

def hinton(W, maxWeight=None):
    """
    reference: http://wiki.scipy.org/Cookbook/Matplotlib/HintonDiagrams
    Draws a Hinton diagram for visualizing a weight matrix. 
    Temporarily disables matplotlib interactive mode if it is on, 
    otherwise this takes forever.
    """
    reenable = False
    if P.isinteractive():
        P.ioff()
    P.clf()
    height, width = W.shape
    if not maxWeight:
        maxWeight = 2**N.ceil(N.log(N.max(N.abs(W)))/N.log(2))

    P.fill(N.array([0,width,width,0]),N.array([0,0,height,height]),'gray')
    P.axis('off')
    P.axis('equal')
    for x in range(0,width-1):
        for y in range(0,height-1):
            _x = x+1
            _y = y+1
            w = W[y,x]
            if w > 0:
                _blob(_x - 0.5, height - _y + 0.5, min(1,w/maxWeight),'white')
            elif w < 0:
                _blob(_x - 0.5, height - _y + 0.5, min(1,-w/maxWeight),'black')
    if reenable:
        P.ion()
    P.show()


def loadResultsIntoMatrix(experiment, numberOfObervations, maxMeanDeathperRun):
    '''
    @param: experiment: either select "table" or "MLP"
    @param: numberOfObervations: how many Do You want to get?
    @param: maxMeanDeathperRun: cut off, only smaller values are loaded into frame    
    '''
    from pymongo import ASCENDING
    mcl=pymongo.MongoClient()
    db=mcl.Client[experiment]
    alive_population=db.find({"fitness":{"$lt":maxMeanDeathperRun}}).sort([('fitness', ASCENDING)])
    limited=alive_population.limit(numberOfObervations)
    print("number of obervations limited to: ", limited.explain().get("n"))
        # Delete the _id

    pa=pandas.DataFrame(list(limited))
    return pa


def printCorrelation(experiment='MLP', numberOfObervations=1000, maxMeanDeathperRun=1):
    pa=loadResultsIntoMatrix(experiment, numberOfObervations, maxMeanDeathperRun)
    
    hinton(pa.as_matrix(columns=['discountFactor', 'fitness', 'hiddenLayerSize', 'learningRate', 'momentum', 'punishment', 'reliability', 'reward', 'weightInitMin']))
def appendTestparameters(punishment=-1):
    param_list=[]
    for k in range(3):
        par=PM.Properties()
        par.paramDict.update({"punishment":punishment})
        param_list.append(par)
    sarsa_list=[]
    sarsa_list.append(sarsa.Separated)
    sarsa_list.append(sarsa.table)
    sarsa_list.append(sarsa.MLP) 
    testParamerts(param_list, sarsa_list)
    return 0
def testParamerts(paramList, algolist):
    '''
    @param paramListas dicts for the PropertiesManager
    @return :list of pandas Series  
    ''' 
    '''lets let the top n run for 10runs and see their fitness'''
    assert len(paramList)==len(algolist), "length of parameter and algos have to be the same"
    mcl=pymongo.MongoClient()
    db=mcl.Client['xpl_tanh']
    for (par,algo) in zip(paramList, algolist):
        sa=algo(par.getReflection())
        fitness=sa.run()
        par.paramDict.update({"fitness":fitness, "algo":algo.__name__})
        db.insert(par.to_json())

def multitask_appendTestparameters(punish_list=[-0.1,-1,-10,-100]):
    from multiprocessing import Pool
    import multiprocessing
    pool= Pool(processes=(multiprocessing.cpu_count() - 3))
    workerList=[]
    for individual in punish_list:
        workerList.append(individual)
        #manages processes
    pool.map(appendTestparameters, workerList)