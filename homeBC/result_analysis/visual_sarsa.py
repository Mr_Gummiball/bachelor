'''created jan 2015 by jonas'''
from matplotlib import pyplot as plt
import pymongo

import BackProp as BP
import PropertiesManager as PM
import matplotlib as mpl
import numpy as N
import sarsa
import transferFuncCollection as TF
from resultMath import GradientMaps



def create_Boxplot(collection):
    '''
    from http://blog.bharatbhole.com/creating-boxplots-with-matplotlib/
    
    creates a bar plot for each algorithm type, with 6 bars at different ticks. uses the error lines from the collection
    '''
    from pymongo import ASCENDING
    ## agg backend is used to create plot as a .png file
    mpl.use('agg')
    mcl=pymongo.MongoClient()
    db=mcl.Client['JS_bachelor']
    col=db[collection]
    #table
    sa=sarsa.table(PM.Properties().getReflection())
    search_tab={"algo":sa.__str__()[1:-25]}
    
    table_cursor=col.find(search_tab).sort([('fitness', ASCENDING)]).limit(5)
    table_err_list=[]
    for i in table_cursor:
        table_err_list.append(i.get('errors')[:16:])
    
            

class VisualSarsa(object):
    '''Observer of sarsa'''
    def __init__(self,name=None):
        self.name = name
        self.target=None
        self.lastPost=None
        self.currentPos=None
        self.deathMap=None
        self.graMap=GradientMaps()
        self.fig, (self.ax1, self.ax2) = plt.subplots(1,2)
        self.fig.suptitle('The movement and the death map')
        self.colorbar=None
        plt.grid(True,color='black')
        plt.ion()
        
    def showDeathMap(self, deathMap):
        '''probably not necessary'''
        #plt.clf()
        self.img2 =self.ax2.imshow(self.deathMap,origin='lower',interpolation='nearest' )
        #plt.imshow(self.oldVisited*0.5, origin='lower')
        self.ax2.set_title(str('DeathMap. Max= '+ str(self.deathMap.max())))
        plt.draw()
        
    def listenTotargetReached(self, arg):
        #fill death
        self.img.set_data(self.pos*3+ self.target)
        #plt.gca().invert_xaxis()
        plt.draw()
        
    def showInitPosition(self,target):

        self.deathMap=N.zeros(shape=target.shape)
        cmap = mpl.colors.LinearSegmentedColormap.from_list('my_colormap',
                                           ['white', 'blue','black','red'],
                                           256)
        self.img = self.ax1.imshow(self.target,interpolation='nearest',origin='lower')#,
                    #cmap = cmap,)
        self.ax1.set_title("Movement of the individual")        
        if self.colorbar is not None:
            None
        else:
            self.colorbar=True
            mapable=self.target-1
            asd = self.ax2.imshow(mapable)
            plt.colorbar(asd,cmap=cmap)
            self.ax2.imshow(self.target*0)
        plt.show()
        
        
    def updatePosVisual(self, pos_array):
        self.img.set_data(pos_array*2+self.target+self.last_pos+self.deathMap)
        #plt.gca().invert_xaxis()
        plt.draw()
        
    def listenToTarget(self, arg, target):

        self.target=target*5
        self.showInitPosition(self.target)
        self.last_pos=self.target*0
        
    def listenToDeath(self, arg, arrayWhereDeathEQone):
        self.deathMap+=arrayWhereDeathEQone
        self.showDeathMap(self.deathMap)
        
    def listenToPos(self, arg, pos):
        self.pos=pos
        if self.graMap.gradientMap_s is not None:
            qval=self.graMap.gradientMap_s+self.graMap.gradientMap_n+self.graMap.gradientMap_w+self.graMap.gradientMap_e
            qval*2
            self.updatePosVisual(pos+qval)
            #reset q-values
            self.graMap.gradientMap_s[:]=0
            self.graMap.gradientMap_e[:]=0
            self.graMap.gradientMap_n[:]=0
            self.graMap.gradientMap_w[:]=0
        else:
            self.updatePosVisual(pos)
        self.last_pos=self.pos
        
def runVisSarsa():
    vis=VisualSarsa('Visual_sarsa')
    params=PM.Properties(random=False)
    params.paramDict["danger"]=True
    sa=sarsa.MLP(params.paramDict)
    wo=sa.interface
    '''init oberserables'''
    sa.q_values+=vis.graMap.listenToValues
    wo.pos += vis.listenToPos
    wo.initPos+=vis.listenToTarget
    wo.deathTick +=vis.listenToDeath
    wo.targetTick+=vis.listenTotargetReached
    sa.interface=wo
    sa.run()
    

class Insert_Error_Line(object):

    def __init__(self, par, algo=sarsa.MLP,collection='test'):
        '''
        @param danger: if True, agent encounters death upon rim.
        @param algo: sarsa algorithm object
        '''
        self.error_list=[]
        self.deathTiccc=[]
        assert type(par)==type(PM.Properties()), 'maybe mixed up paramter order?'
        Prop=par.getCopy()
        self.sa=algo(Prop.getReflection())
        self.item=Prop.to_json()
        #print("Reflection: ", Prop.to_json())
        if par.paramDict.get('danger')==True:
            self.sa.punish+=self.listenToDeath
        if type(self.sa)==type(sarsa.Separated(par.getReflection())):
            self.sa.wc.setError+=self.listenToError
        else:
            self.sa.wc.set_NN_Error+=self.listenToError

        fitness=self.sa.run()
        self.item.update({"fitness":fitness})
        self.item.update({"algo":self.sa.__str__()[1:-25]})
        #conncet
        mcl=pymongo.MongoClient()
        db=mcl.Client['JS_bachelor']
        col=db[collection]
        tick_length=int(len(self.error_list)/100)
        self.aggregate(tick_length)
        
        if len(self.deathTiccc)>1:
            death_Err=[sum(self.deathTiccc[z:z+tick_length:]) for z in range(int(len(self.deathTiccc)/tick_length))]
            self.item.update({"deaths":death_Err})
        #print("insert Item ", self.item)
        col.insert(self.item)
        #return self.item
        return None
        
        
        
    def aggregate(self, tick_length):
        assert len(self.error_list)>1 , 'error list is empty'
        try:
            mlp_err=[sum(self.error_list[x:x+tick_length:][0]) for x in range(int(len(self.error_list)/tick_length))]
            fear_err=[sum(self.error_list[y:y+tick_length:][1]) for y in range(int(len(self.error_list)/tick_length))]
            self.item.update({"errors mlp":mlp_err})
            self.item.update({"errors fear":fear_err})
        except:
            mlp_err=[sum(self.error_list[x:x+tick_length:]) for x in range(int(len(self.error_list)/tick_length))]
            self.item.update({"errors":mlp_err})

        
    def listenToDeath(self, arg, deathBool):
        if deathBool:
            self.deathTiccc.append(1)
        else:
            self.deathTiccc.append(0)
    def listenToError(self,arg, error):
        self.error_list.append(error)
        
def paramerschmiede():
    exp_list=[]
    
    par=PM.Properties()
    par.paramDict.update({"transferFunc":TF.tanghyper})
    exp_list.append((par, sarsa.Separated, '/home/jonas/Pictures/freSam/e1'))

    par_2=PM.Properties()
    par_2.paramDict.update({"punishment": -100})
    par_2.paramDict.update({"reward": 10})
    par_2.paramDict.update({"discountFactor":0.5})
    par_2.paramDict.update({"transferFunc":TF.tanghyper})
    exp_list.append((par_2, sarsa.Separated ,'/home/jonas/Pictures/freSam/e2'))
    

    par_4=PM.Properties()
    par_4.paramDict.update({"punishment": -50})
    par_4.paramDict.update({"learningRate": 0.3})
    par_4.paramDict.update({"hiddenLayerSize":20})
    par_4.paramDict.update({"transferFunc":TF.tanghyper})
    exp_list.append((par_4, sarsa.MLP, '/home/jonas/Pictures/freSam/e3'))
    
    par_4_2=PM.Properties()
    par_4_2.paramDict.update({"punishment": -1})
    par_4_2.paramDict.update({"learningRate": 0.2})
    par_4_2.paramDict.update({"hiddenLayerSize":3})
    par_4_2.paramDict.update({"reward":5})
    par_4_2.paramDict.update({"transferFunc":TF.tanghyper})
    exp_list.append((par_4_2, sarsa.Separated, '/home/jonas/Pictures/freSam/e4'))
    
    par_5=PM.Properties()
    par_5.paramDict.update({"punishment": -2})
    par_5.paramDict.update({"learningRate": 0.06})
    par_5.paramDict.update({"hiddenLayerSize":20})
    par_5.paramDict.update({"discountFactor":0.75})
    exp_list.append((par_5, sarsa.Separated, '/home/jonas/Pictures/freSam/e5'))
    
    par_6=PM.Properties()
    par_6.paramDict.update({"punishment": -40})
    par_6.paramDict.update({"reward": 1})
    par_6.paramDict.update({"discountFactor":0.5})
    par_6.paramDict.update({"hiddenLayerSize":8})
    exp_list.append((par_6, sarsa.Separated,'/home/jonas/Pictures/freSam/e6'))
    
    par_7=PM.Properties()
    par_7.paramDict.update({"punishment": -0.001})
    par_7.paramDict.update({"discountFactor":0.9})
    par_7.paramDict.update({"reward":100})
    par_7.paramDict.update({"hiddenLayerSize":8})
    exp_list.append((par_7, sarsa.Separated, '/home/jonas/Pictures/freSam/e7'))
    
    par_8=PM.Properties()
    par_8.paramDict.update({"punishment": -0.001})
    par_8.paramDict.update({"discountFactor":0.9})
    par_8.paramDict.update({"reward":1})
    par_8.paramDict.update({"hiddenLayerSize":8})
    exp_list.append((par_8, sarsa.Separated, '/home/jonas/Pictures/freSam/e8'))

    from multiprocessing import Pool
    import multiprocessing
    pool= Pool(processes=(multiprocessing.cpu_count()-2))
        #manages processes
    pool.map(mapRun, exp_list)


def runner(params):
    col_2='results_tan_vertauscht'
    col='results_tanh2'
    col_='debug'
    Insert_Error_Line(par=params[0], algo=params[1], collection=col)
    
def mapRun(params):
    from result_analysis import resultMath as RM
    RM.printGradientMap(par=params[0], algo=params[1], toSave=params[2])
if __name__=='__main__':
    mpl.use('agg')
    paramerschmiede()
    '''
    par_=PM.Properties()
    par_.paramDict.update({"punishment:": -114})
    par_.paramDict.update({"hiddenLayerSize":7})
    par_.paramDict.update({"runs:": 200})
    Insert_Error_Line(par=par_, algo=sarsa.table, collection='debug')
    
    import sarsa
    import PropertiesManager as PM
    import transferFuncCollection as TF
    col='mind'
    from visual_sarsa import Insert_Error_Line as IE
    par_5=PM.Properties()
    par_5.paramDict.update({"punishment":-4})
    par_5.paramDict.update({"punishment":2})
    par_5.paramDict.update({"learningRate": 0.132}) 
    par_5.paramDict.update({"hiddenLayerSize":10})
    par_5.paramDict.update({"transferFunc":TF.sgm})
    ans_item=IE(par=par_5, algo=sarsa.Separated ,collection=col)
    
    '''